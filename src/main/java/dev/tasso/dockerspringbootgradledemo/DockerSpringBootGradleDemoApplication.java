package dev.tasso.dockerspringbootgradledemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerSpringBootGradleDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerSpringBootGradleDemoApplication.class, args);
	}

}
